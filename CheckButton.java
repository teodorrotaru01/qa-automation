package Test1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;

public class CheckButton {
	public static void main(String[] args) throws InterruptedException {
		
	System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	
	driver.manage().window().maximize();
	driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
	
	driver.get("https://politrip.com/account/sign-up/");
	
	Thread.sleep(6000);
	driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
	
	WebElement buttonPush = driver.findElement(By.xpath("//*[@id=\"sign-up-heard-input\"]"));
	buttonPush.click();
	
	Thread.sleep(5000);
	Select button = new Select(buttonPush); 
	button.selectByVisibleText("Social networks");
	
	WebElement selectedText = button.getFirstSelectedOption();
	System.out.println("I selected " + selectedText.getText());
	
	driver.quit();
	}
}