package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNG {

	WebDriverWait wait;
	WebDriver driver;
	String url;
	@BeforeTest
	public void beforeTest()
	{
	}
	
	@AfterTest
	public void afterTest() {
	driver.quit();}
	
	@Test
	
	public void verifylink() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		driver = new ChromeDriver();
		
		url = "https://politrip.com/account/sign-up" ;

	driver.get(url);
	driver.manage().window().maximize();
	driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
	
	String urlWeb = driver.getCurrentUrl();
	
	
	Assert.assertEquals(urlWeb, url);
	}
	@Test
	public void WrongCharacter() throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		driver.get("https://politrip.com/account/sign-up/");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cookiescript_accept\"]")));
		driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
		
		
		driver.findElement(By.id("first-name")).sendKeys("1235");
		driver.findElement(By.id("last-name")).sendKeys("1235");
		driver.findElement(By.id("email")).sendKeys("testmai");
		driver.findElement(By.xpath("//*[@id=\"sign-up-password-input\"]")).sendKeys("test");
		driver.findElement(By.xpath("//*[@id=\"sign-up-confirm-password-input\"]")).sendKeys("paroa12");
		
		driver.findElement(By.xpath("//*[@id=\" qa_loader-button\"]/span")).click();
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sign-up-confirm-password-input\"]")));
		
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-first-name-div\"]/app-form-control-error-message/em/span")).getText(), "Wrong characters or format");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-last-name-div\"]/app-form-control-error-message/em/span")).getText(), "Wrong characters or format");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-email-div\"]/app-form-control-error-message/em/span")).getText(), "Please enter a valid email address");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-password-div\"]/app-form-control-error-message/em/span")).getText(), "Password must contain at least 8 characters, 1 uppercase letter, 1 lowercase letter and 1 digit");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-confirm-password-div\"]/app-form-control-error-message/em/span")).getText(), "Passwords must match");
		
	}
	@Test
	public void CheckButton() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cookiescript_accept\"]")));
		driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
		
		WebElement buttonPush = driver.findElement(By.xpath("//*[@id=\"sign-up-heard-input\"]"));
		buttonPush.click();
		Select button = new Select(buttonPush); 
		button.selectByVisibleText("Social networks");
		WebElement selectedText = button.getFirstSelectedOption();
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-heard-input\"]/option[3]")).getText(), selectedText.getText());	
	}
	
	@Test
	public void EmptyField() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cookiescript_accept\"]")));
		driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
		
		driver.findElement(By.id("first-name")).sendKeys("");
		driver.findElement(By.id("last-name")).sendKeys("");
		driver.findElement(By.id("email")).sendKeys("");
		driver.findElement(By.xpath("//*[@id=\"sign-up-password-input\"]")).sendKeys("");
		driver.findElement(By.id("sign-up-confirm-password-input")).sendKeys("");
		driver.findElement(By.xpath("//*[@id=\" qa_loader-button\"]/span")).click();
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sign-up-confirm-password-input\"]")));
		
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-first-name-div\"]/app-form-control-error-message/em/span")).getText(), "This field can not be empty");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-last-name-div\"]/app-form-control-error-message/em/span")).getText(), "This field can not be empty");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-email-div\"]/app-form-control-error-message/em/span")).getText(), "This field can not be empty");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-password-div\"]/app-form-control-error-message/em/span")).getText(), "This field is required");
		Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"sign-up-confirm-password-div\"]/app-form-control-error-message/em/span")).getText(), "This field is required");
		
	}
	@Test
	public void SignUp() throws InterruptedException{
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cookiescript_accept\"]")));
		driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
		
		driver.findElement(By.id("first-name")).sendKeys("Teodor");
		driver.findElement(By.id("last-name")).sendKeys("Rotaru");
		driver.findElement(By.id("email")).sendKeys("testmai97@yahoo.com");
		driver.findElement(By.xpath("//*[@id=\"sign-up-password-input\"]")).sendKeys("Test1234");
		driver.findElement(By.xpath("//*[@id=\"sign-up-confirm-password-input\"]")).sendKeys("Test1234");
		driver.findElement(By.xpath("//*[@id=\" qa_loader-button\"]/span")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"sign-up-component-div\"]/div/p[1]")));
		Boolean Display = driver.findElement(By.xpath("//*[@id=\"sign-up-component-div\"]/div/p[1]")).isDisplayed();
		Assert.assertTrue(Display);
		
	}
	
	@Test
	public void FindElem() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		Boolean Element1 = driver.findElement(By.xpath("//*[@id=\"qa_header-home\"]/span[2]")).isDisplayed();
		Boolean Element2 = driver.findElement(By.xpath("//*[@id=\"td-cover-block-0\"]/div/div/div/div/h1")).isDisplayed();
		Boolean Element3 = driver.findElement(By.xpath("//*[@id=\"qa_header-login\"]")).isDisplayed();
		
		Assert.assertTrue(Element1);
		Assert.assertTrue(Element2);
		Assert.assertTrue(Element3);
	}
}
	
