package Test1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class EmptyFields {
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		Thread.sleep(6000);
		driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("first-name")).sendKeys("");
		driver.findElement(By.id("last-name")).sendKeys("");
		driver.findElement(By.id("email")).sendKeys("");
		driver.findElement(By.xpath("//*[@id=\"sign-up-password-input\"]")).sendKeys("");
		driver.findElement(By.id("sign-up-confirm-password-input")).sendKeys("");
		driver.findElement(By.xpath("//*[@id=\" qa_loader-button\"]/span")).click();
		
		
		Thread.sleep(5000);
		System.out.println(driver.findElement(By.xpath("//*[@id=\"sign-up-first-name-div\"]/app-form-control-error-message/em/span")).getText());
		System.out.println(driver.findElement(By.xpath("//*[@id=\"sign-up-last-name-div\"]/app-form-control-error-message/em/span")).getText());
		System.out.println(driver.findElement(By.xpath("//*[@id=\"sign-up-email-div\"]/app-form-control-error-message/em/span")).getText());
		System.out.println(driver.findElement(By.xpath("//*[@id=\"sign-up-password-div\"]/app-form-control-error-message/em/span")).getText());
		System.out.println(driver.findElement(By.xpath("//*[@id=\"sign-up-confirm-password-div\"]/app-form-control-error-message/em/span")).getText());
		
		driver.quit();
}
}
