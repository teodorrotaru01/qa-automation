package Test1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElements {
public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		
		if(driver.findElements(By.xpath("//*[@id=\"qa_header-home\"]/span[2]")).size() != 0) {
			System.out.println("Home button exists!");
		}else {
			System.out.println("Home button doesn't exist!");
		}
		
		if(driver.findElements(By.xpath("//*[@id=\"td-cover-block-0\"]/div/div/div/div/h1")).size() != 0) {
			System.out.println("The page has download option!");
		}else {
			System.out.println("There is no download option!");
		}
		
		if(driver.findElements(By.xpath("//*[@id=\"qa_header-login\"]")).size() != 0) {
			System.out.println("Log-in button exists!");
		}else {
			System.out.println("There is no log-in button!");
		}
		driver.quit();
}
}
