package Test1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SignUpSucced {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "E:\\senium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		
		driver.get("https://politrip.com/account/sign-up/");
		
		Thread.sleep(6000);
		driver.findElement(By.xpath("//*[@id=\"cookiescript_accept\"]")).click();
		
		driver.findElement(By.id("first-name")).sendKeys("Teodor");
		driver.findElement(By.id("last-name")).sendKeys("Rotaru");
		driver.findElement(By.id("email")).sendKeys("testmai97@yahoo.com");
		driver.findElement(By.xpath("//*[@id=\"sign-up-password-input\"]")).sendKeys("Test1234");
		driver.findElement(By.xpath("//*[@id=\"sign-up-confirm-password-input\"]")).sendKeys("Test1234");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\" qa_loader-button\"]/span")).click();
		Thread.sleep(5000);
		if(driver.findElements(By.xpath("//*[@id=\"sign-up-component-div\"]/div/p[1]")).size() != 0) {
			System.out.println("Sign-up succed!");
		}else {
			System.out.println("Sign-up faied!");
		}
		driver.quit();
}
}
